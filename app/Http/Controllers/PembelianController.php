<?php

namespace App\Http\Controllers;

use App\Models\master_barang;
use App\Models\transaksi_pembelian;
use App\Models\transaksi_pembelian_barang;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PembelianController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function indexPembelian()
    {
        $title = "List Pembelian";

        $pembelian = transaksi_pembelian_barang::with(['pembelian', 'barang'])->paginate(5)->withQueryString();

        return view('pembelian.index', compact(['title', 'pembelian']));
    }


    public function createPembelianBarang()
    {
        $title = "Formulir Pembelian Barang";

        $barangs = master_barang::get();

        return view('pembelian.create', compact(['title', 'barangs']));
    }

    public function storePembelianBarang(Request $request)
    {
        $rule = [
            'jumlah'        => 'required|numeric|min:0',
            'hargaSatuan'   => 'required|numeric|min:0',
            'hargaTotal'   => 'required|numeric|min:0',
        ];

        $validator = Validator::make($request->all(), $rule);

        if ($validator->fails()) {
            return redirect()->route('pembelian.create', $request->idBarang)
                ->withErrors($validator)
                ->withInput();
        }

        $dataPembelian = [
            'total_harga' => $request->hargaTotal
        ];

        transaksi_pembelian::create($dataPembelian);

        $pembelian = transaksi_pembelian::latest()->first();

        $dataPembelianBarang = [
            'transaksi_pembelian_id'    => $pembelian->id,
            'master_barang_id'          => $request->idBarang,
            'jumlah'                    => $request->jumlah,
            'harga_satuan'              => $request->hargaSatuan
        ];

        transaksi_pembelian_barang::create($dataPembelianBarang);

        toast('Data pembelian' . $request->namaBarang . ' berhasil ditambahkan', 'success');
        return redirect()->route('pembelian.index');
    }
}
