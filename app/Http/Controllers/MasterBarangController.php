<?php

namespace App\Http\Controllers;

use App\Models\master_barang;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use RealRashid\SweetAlert\Facades\Alert;

class MasterBarangController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $title = "List Barang";

        $master_barang = master_barang::paginate(5)->withQueryString();

        return view('masterBarang.index', compact(['title', 'master_barang']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $title = "Tambahkan Barang";
        return view('masterBarang.create', compact(['title']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $rule = [
            'namaBarang'    => 'required|min:4',
            'hargaSatuan'   => 'required|numeric|min:0',
            'jumlah'        => 'required|numeric|min:0',
            'hargaTotal'    => 'required|numeric|min:0',
        ];

        $validator = Validator::make($request->all(), $rule);

        if ($validator->fails()) {
            return redirect()->route('barang.create')
                ->withErrors($validator)
                ->withInput();
        }

        $data = [
            'nama_barang' => $request->namaBarang,
            'harga_satuan' => $request->hargaSatuan
        ];

        master_barang::create($data);
        toast('Data barang berhasil ditambahkan', 'success');
        return redirect()->route('barang.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $barang = master_barang::findOrFail($id);
        $title = "ubah barang";

        return view('masterBarang.edit', compact(['barang', 'title']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $barang = master_barang::find($id);

        $rule = [
            'namaBarang'    => 'required|min:4',
            'hargaSatuan'   => 'required|numeric|min:0'
        ];

        $validator = Validator::make($request->all(), $rule);

        if ($validator->fails()) {
            return redirect()->route('barang.edit', $id)
                ->withErrors($validator)
                ->withInput();
        }

        $data = [
            'nama_barang' => $request->namaBarang,
            'harga_satuan' => $request->hargaSatuan
        ];

        master_barang::where('id', $id)->update($data);
        toast('Data ' . $barang->nama_barang . ' berhasil diperbaharui', 'info');
        return redirect()->route('barang.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $barang = master_barang::find($id);

        toast('Data ' . $barang->nama_barang . ' berhasil dihapuskan', 'error');
        master_barang::where('id', $id)->delete();
        return redirect()->route('barang.index');
    }
}
