<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class master_barang extends Model
{
    use HasFactory;

    protected $guard = [];

    protected $table = 'master_barangs';

    protected $fillable = [
        'nama_barang',
        'harga_satuan'
    ];
}
