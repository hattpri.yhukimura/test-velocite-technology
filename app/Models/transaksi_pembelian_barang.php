<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class transaksi_pembelian_barang extends Model
{
    use HasFactory;

    protected $fillable = [
        'transaksi_pembelian_id',
        'master_barang_id',
        'jumlah',
        'harga_satuan'
    ];

    function pembelian()
    {
        return $this->belongsTo(transaksi_pembelian::class, 'transaksi_pembelian_id');
    }

    function barang()
    {
        return $this->belongsTo(master_barang::class, 'master_barang_id');
    }
}
