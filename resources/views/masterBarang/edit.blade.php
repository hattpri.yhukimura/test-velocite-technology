@extends('layouts.template')

@section('content')
<div class="container-fluid px-4">
    <h1 class="mt-4 text-uppercase">{{$title}}</h1>
    <ol class="breadcrumb mb-3">
        <li class="breadcrumb-item active text-uppercase">{{$title}}</li>
    </ol>
    <hr>
    <div class="row mb-2">
        <div class="col-12">
            <a href="{{ route('barang.index') }}" class="btn btn-sm btn-outline-secondary">
                kembali
            </a>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <div class="card text-bg-light">
                <div class="card-header text-center text-uppercase">
                    <h5>Formulir Penambahan barang</h5>
                </div>
                <div class="card-body">
                    <form action="{{ route('barang.update', $barang->id) }}" method="POST">
                        @csrf
                        @method('PUT')
                        <div class="mb-3">
                            <label for="namaBarang" class="form-label">Nama Barang</label>
                            <input type="text" class="form-control" id="namaBarang" aria-describedby="namaBarang" name="namaBarang" value="{{ $barang->nama_barang }}">
                            @error('namaBarang')
                            <div id="namaBarang" class="form-text text-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="mb-3">
                            <label for="hargaSatuan" class="form-label">Harga Satuan</label>
                            <input type="text" class="form-control" id="hargaSatuan" name="hargaSatuan" value="{{ $barang->harga_satuan }}">
                            @error('hargaSatuan')
                            <div id="hargaSatuan" class="form-text text-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <button type="submit" class="btn btn-sm btn-outline-primary">tambahkan</button>
                        <a href="{{ route('barang.index') }}" class="btn btn-sm btn-outline-secondary">
                            tutup
                        </a>
                    </form>
                </div>
            </div>
        </div>
    </div>

</div>
@endsection