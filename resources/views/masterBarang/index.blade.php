@extends('layouts.template')

@section('content')
<div class="container-fluid px-4">
    <h1 class="mt-4 text-uppercase">{{$title}}</h1>
    <ol class="breadcrumb mb-3">
        <li class="breadcrumb-item active text-uppercase">{{$title}}</li>
    </ol>
    <hr>
    <div class="row mb-2">
        <div class="col-12">
            <a href="{{ route('barang.create') }}" class="btn btn-sm btn-outline-info">
                formulir barang
            </a>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <table class="table table-bordered table-condensed table-hover">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Nama Barang</th>
                        <th>Harga Satuan</th>
                        <th>Created</th>
                        <th>Updated</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($master_barang as $key => $barang)
                    <tr>
                        <td>{{ $key + $master_barang->firstItem() }}</td>
                        <td>{{ $barang->nama_barang }}</td>
                        <td>{{ $barang->harga_satuan }}</td>
                        <td>{{ $barang->created_at }}</td>
                        <td>{{ $barang->updated_at }}</td>
                        <td>
                            <a href="{{ route('pembelian.create', $barang->id) }}" class="btn btn-sm btn-outline-secondary">form pembelian</a>
                            <a href="{{ route('barang.edit', $barang->id) }}" class="btn btn-outline-warning btn-sm">
                                ubah
                            </a>
                            <button href="{{ route('barang.destroy', $barang->id) }}" class="btn btn-outline-danger btn-sm" id="delete">
                                hapus
                            </button>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            <span class="float-end">{{ $master_barang->links() }}</span>
        </div>
    </div>
</div>

<form action="" method="post" id="deleteForm">
    @csrf
    @method("DELETE")
    <input type="submit" value="Hapus" style="display: none">
</form>


<script src="https://code.jquery.com/jquery-3.6.1.js" integrity="sha256-3zlB5s2uwoUzrXK3BT7AX3FyvojsraNFxCc2vC/7pNI=" crossorigin="anonymous"></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@9/dist/sweetalert2.min.js"></script>

<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>

<script>
    $('button#delete').on('click', function(e) {
        e.preventDefault();
        var href = $(this).attr('href');

        const swalWithBootstrapButtons = Swal.mixin({
            customClass: {
                confirmButton: 'btn btn-sm btn-success',
                cancelButton: 'btn btn-sm btn-danger'
            },
            buttonsStyling: false
        });

        Swal.fire({
            title: 'Apakah kamu yakin?',
            text: "Anda tidak akan dapat mengembalikannya!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Ya, Hapus ini!',
            cancelButtonText: 'Tidak, Batalkan!',
        }).then((result) => {
            if (result.value) {
                document.getElementById('deleteForm').action = href;
                document.getElementById('deleteForm').submit();

                swalWithBootstrapButtons.fire(
                    'Terhapus!',
                    'Data berhasil dihapus dari sistem.',
                    'success'
                )
            } else if (
                result.dismiss === Swal.DismissReason.cancel
            ) {
                swalWithBootstrapButtons.fire(
                    'Dibatalkan',
                    'Data berhasil diamakan :)',
                    'error'
                )
            }
        });
    });
</script>
@endsection