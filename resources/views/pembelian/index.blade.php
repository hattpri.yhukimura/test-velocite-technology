@extends('layouts.template')

@section('content')
<div class="container-fluid px-4">
    <h1 class="mt-4 text-uppercase">{{$title}}</h1>
    <ol class="breadcrumb mb-3">
        <li class="breadcrumb-item active text-uppercase">{{$title}}</li>
    </ol>
    <hr>
    <div class="row mb-2">
        <div class="col-12">
            <a href="{{ route('pembelian.create') }}" class="btn btn-sm btn-outline-info">
                formulir pembelian
            </a>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <table class="table table-bordered table-condensed table-hover">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Nama barang</th>
                        <th>Jumlah</th>
                        <th>Harga Satuan</th>
                        <th>Created</th>
                        <th>Total</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($pembelian as $key => $beli)
                    <tr>
                        <td>{{ $key + $pembelian->firstItem() }}</td>
                        <td>{{ $beli->barang->nama_barang }}</td>
                        <td>{{ $beli->jumlah }}</td>
                        <td>{{ $beli->barang->harga_satuan }}</td>
                        <td>{{ $beli->barang->harga_satuan * $beli->jumlah }}</td>
                        <td>{{ $beli->pembelian->created_at }}</td>

                    </tr>
                    @endforeach
                </tbody>
            </table>
            <span class="float-end">{{ $pembelian->links() }}</span>
        </div>
    </div>
</div>

<form action="" method="post" id="deleteForm">
    @csrf
    @method("DELETE")
    <input type="submit" value="Hapus" style="display: none">
</form>


<script src="https://code.jquery.com/jquery-3.6.1.js" integrity="sha256-3zlB5s2uwoUzrXK3BT7AX3FyvojsraNFxCc2vC/7pNI=" crossorigin="anonymous"></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@9/dist/sweetalert2.min.js"></script>

<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>

<script>
    $('button#delete').on('click', function(e) {
        e.preventDefault();
        var href = $(this).attr('href');

        const swalWithBootstrapButtons = Swal.mixin({
            customClass: {
                confirmButton: 'btn btn-sm btn-success',
                cancelButton: 'btn btn-sm btn-danger'
            },
            buttonsStyling: false
        });

        Swal.fire({
            title: 'Apakah kamu yakin?',
            text: "Anda tidak akan dapat mengembalikannya!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Ya, Hapus ini!',
            cancelButtonText: 'Tidak, Batalkan!',
        }).then((result) => {
            if (result.value) {
                document.getElementById('deleteForm').action = href;
                document.getElementById('deleteForm').submit();

                swalWithBootstrapButtons.fire(
                    'Terhapus!',
                    'Data berhasil dihapus dari sistem.',
                    'success'
                )
            } else if (
                result.dismiss === Swal.DismissReason.cancel
            ) {
                swalWithBootstrapButtons.fire(
                    'Dibatalkan',
                    'Data berhasil diamakan :)',
                    'error'
                )
            }
        });
    });
</script>
@endsection