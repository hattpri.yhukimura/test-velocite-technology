@include('layouts.partials.header')

<body class="sb-nav-fixed">
    @include('layouts.partials.navbar')
    <div id="layoutSidenav">
        <div id="layoutSidenav_nav">
            @include('layouts.partials.navbar-side')
        </div>
        <div id="layoutSidenav_content">
            @include('sweetalert::alert')
            <main>
                @yield('content')
            </main>
            @include('layouts.partials.footer')

        </div>
    </div>
    @include('layouts.partials.scripts')
</body>

</html>